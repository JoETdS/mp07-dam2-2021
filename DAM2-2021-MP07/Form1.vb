﻿Imports MySql.Data.MySqlClient
Public Class Form1

    Dim connexio As MySqlConnection
    Dim query As String

    Private Sub boto_dades_Click(sender As Object, e As EventArgs) Handles boto_dades.Click
        Dim alumne As Alumne
        connectar()
        mostrarAlumne()
        afegirAlumne()
        desconnectar()
    End Sub

    Private Sub afegirAlumne()

        Dim nom As String
        nom = text_nom.Text

        'passem un paràmetre com valor de la sentència SQL
        query = "INSERT INTO `alumne` (`nom`, `nota`) VALUES ('" + nom + "', '2')"

        'executem la comanda
        Dim comanda = New MySqlCommand(query, connexio)
        comanda.ExecuteNonQuery()

    End Sub

    Private Sub mostrarAlumne()
        query = "SELECT * FROM alumne"
        'PASSAR DADES DE LA BD A UNA TAULA DE DADES DE VB'
        Dim comanda As New MySqlCommand(query, connexio)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)

        Dim alumne As New Alumne("", 0)
        alumne.setNom(conjunt_dades.Rows(0).Item(1)) 'agafem els valors de la fila 0, columna 1
        alumne.setNota(conjunt_dades.Rows(0).Item(2)) 'agafem els valors de la fila 0, columna 2

        'mostrem els valors a les etiquetes
        etiqueta_nom.Text = alumne.getNom()
        etiqueta_nota.Text = alumne.getNota()

    End Sub


    Private Sub connectar()
        Try
            connexio = New MySqlConnection()
            connexio.ConnectionString = "server=localhost;user id=root;password=alumne;database=alumnes"
            connexio.Open()
            MessageBox.Show("Connectat")
        Catch
            MessageBox.Show("Error")
        End Try
    End Sub

    Private Sub desconnectar()
        connexio.Close()
    End Sub

End Class
