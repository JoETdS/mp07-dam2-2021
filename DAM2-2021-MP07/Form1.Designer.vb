﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.boto_dades = New System.Windows.Forms.Button()
        Me.etiqueta_nom = New System.Windows.Forms.Label()
        Me.etiqueta_nota = New System.Windows.Forms.Label()
        Me.text_nom = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'boto_dades
        '
        Me.boto_dades.Location = New System.Drawing.Point(133, 102)
        Me.boto_dades.Name = "boto_dades"
        Me.boto_dades.Size = New System.Drawing.Size(233, 62)
        Me.boto_dades.TabIndex = 0
        Me.boto_dades.Text = "mostrar dades"
        Me.boto_dades.UseVisualStyleBackColor = True
        '
        'etiqueta_nom
        '
        Me.etiqueta_nom.AutoSize = True
        Me.etiqueta_nom.Location = New System.Drawing.Point(114, 205)
        Me.etiqueta_nom.Name = "etiqueta_nom"
        Me.etiqueta_nom.Size = New System.Drawing.Size(39, 13)
        Me.etiqueta_nom.TabIndex = 1
        Me.etiqueta_nom.Text = "Label1"
        '
        'etiqueta_nota
        '
        Me.etiqueta_nota.AutoSize = True
        Me.etiqueta_nota.Location = New System.Drawing.Point(245, 205)
        Me.etiqueta_nota.Name = "etiqueta_nota"
        Me.etiqueta_nota.Size = New System.Drawing.Size(39, 13)
        Me.etiqueta_nota.TabIndex = 2
        Me.etiqueta_nota.Text = "Label2"
        '
        'text_nom
        '
        Me.text_nom.Location = New System.Drawing.Point(63, 28)
        Me.text_nom.Name = "text_nom"
        Me.text_nom.Size = New System.Drawing.Size(143, 20)
        Me.text_nom.TabIndex = 3
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(548, 373)
        Me.Controls.Add(Me.text_nom)
        Me.Controls.Add(Me.etiqueta_nota)
        Me.Controls.Add(Me.etiqueta_nom)
        Me.Controls.Add(Me.boto_dades)
        Me.Name = "Form1"
        Me.Text = "Calculadora"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents boto_dades As Button
    Friend WithEvents etiqueta_nom As Label
    Friend WithEvents etiqueta_nota As Label
    Friend WithEvents text_nom As TextBox
End Class
