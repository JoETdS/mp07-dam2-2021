﻿Public Class Alumne
    Private nom As String
    Private nota As Int32

    Public Sub New(ByVal nom As String, ByVal nota As Int32)
        Me.nom = nom
        Me.nota = nota
    End Sub

    Public Sub setNom(ByVal nom As String)
        Me.nom = nom
    End Sub

    Public Function getNom() As String
        Return Me.nom
    End Function

    Public Function getNota() As Int32
        Return Me.nota
    End Function

    Public Sub setNota(ByVal nota As Int32)
        Me.nota = nota
    End Sub

End Class
